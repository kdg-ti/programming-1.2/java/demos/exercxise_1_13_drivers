import be.kdg.drivers.Driver;
import be.kdg.drivers.Drivers;

public class DriverRunner {
	private static String[][] champions = {
		{"Germany", "Michael Schumacher"},
		{"Argentina", "Juan Manuel Fangio"},
		{"France", "Alain Prost"},
		{"Germany", "Sebastian Vettel"},
		{"Australia", "Jack Brabham"},
		{"United Kingdom", "Jackie Stewart"},
		{"Austria", "Niki Lauda"},
		{"Brasil", "Nelson Piquet"},
		{"Brasil", "Ayrton Senna"},
		{"United Kingdom", "Lewis Hamilton"},
		{"The Netherlands", "Max Verstappen"}
	};

	private static Integer[][] years = {
		{1994, 1995, 2000, 2001, 2002, 2003, 2004},
		{1951, 1954, 1955, 1956, 1957},
		{1985, 1986, 1993, 1989},
		{2010, 2011, 2012, 2013},
		{1959, 1960, 1966},
		{1969, 1971, 1973},
		{1975, 1977, 1984},
		{1981, 1983, 1987},
		{1988, 1990, 1991},
		{2008, 2014, 2015, 2017, 2018, 2019, 2020},
		{2024, 2023, 2022, 2021}
	};

	public static void main(String[] args) {
		Drivers drivers = new Drivers(champions, years);
		System.out.println(drivers.getDriverByName("Jackie Stewart"));
		printDriver(2000,drivers);
		printDriver(2005,drivers);
	}

	private static void printDriver(int year,Drivers drivers) {
		Driver driver =drivers.getDriverByYear(year);
		System.out.printf("%d champion: %s%n", year, driver!=null?driver:"Not listed");
	}
}
