package be.kdg.drivers;

import java.util.*;

public class Drivers {
	//private List<Driver> drivers =new ArrayList<>();
	private Map<String,Driver> driverByName = new TreeMap<>();
	private Map<Integer,Driver> driverByYear = new TreeMap<>();

	public Drivers(String[][] champions, Integer[][] years) {
		for (int i = 0; i < champions.length; i++) {
			Driver driver= new Driver(champions[i][1],champions[i][0],years[i]);
			driverByName.put(driver.getName(),driver);
			for(int year:driver.getChampionYears()){
				driverByYear.put(year,driver);
			}
		}
	}

	public Driver getDriverByName(String name){
		return driverByName.get(name);
	}

	public Driver getDriverByYear(int year){
		return driverByYear.get(year);
	}
}
