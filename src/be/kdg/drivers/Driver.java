package be.kdg.drivers;

import java.util.*;

public class Driver {
	private String name;
	private String country;
private 	Set <Integer> championYears ;

	public Driver(String name,String country,Set<Integer> championYears) {
		this.name = name;
		this.country = country;
		this.championYears=championYears;
	}

	public Driver(String name, String country, Integer[] years) {
		this.name = name;
		this.country = country;
		championYears = new TreeSet<>();
		championYears.addAll(List.of(years));
	}

	public String getName() {
		return name;
	}

	public String getCountry() {
		return country;
	}

	public Set<Integer> getChampionYears() {
		return championYears;
	}

	@Override
	public String toString() {
		//Jackie Stewart (United Kingdom) 3 times champion [1969, 1971, 1973]
		return String.format("%s (%s) %d times champion %s",name,country,championYears.size(),championYears);
	}
}
